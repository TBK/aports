# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=py3-openssl
_pkgname=pyOpenSSL
pkgver=20.0.0
pkgrel=0
pkgdesc="Python3 wrapper module around the OpenSSL library"
url="https://github.com/pyca/pyopenssl"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-cryptography py3-six"
makedepends="py3-setuptools"
checkdepends="py3-pytest"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!check" # depends on flaky

replaces="py-openssl" # Backwards compatibility
provides="py-openssl=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="26e8a64d53a35434ad4bf5ef50ed5c37d7b92b0db971a2103dcafea56a0b377d67333af630a31405a97cf2cffddde320907b4950ed336a8321391854bcd2c50a  pyOpenSSL-20.0.0.tar.gz"
