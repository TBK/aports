# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=picard
pkgver=2.5.4
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://picard.musicbrainz.org/"
pkgdesc="Official MusicBrainz tagger"
license="GPL-2.0-or-later"
depends="
	chromaprint
	py3-mutagen
	py3-qt5
	"
makedepends="
	gettext
	py3-setuptools
	python3-dev
	"
checkdepends="
	py3-dateutil
	py3-pytest
	"
subpackages="$pkgname-lang"
source="$pkgname-$pkgver.tar.gz::https://github.com/metabrainz/picard/archive/release-$pkgver.tar.gz"
builddir="$srcdir/picard-release-$pkgver"

build() {
	python3 setup.py config
}

check() {
	pytest
}

package() {
	python3 setup.py install \
		--root="$pkgdir" \
		--disable-autoupdate
}

sha512sums="33cb5f494e78527c285fc1a967ea11432ee4bc8ab837cb39c013d7d46d941becb9dca5bb28809e00ea43e0d3b63012710ddf5ec4dfb97bfdb9ea94d61a2b89b0  picard-2.5.4.tar.gz"
